import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({navigation}) => {
    const tasks = [{
        id:1, 
        title: "Ir ao Mercado", 
        date:'2024-02-27', 
        time: '10:00',
        address:"Supermercado SS"
    },
    {       
        id:2, 
        title: "Ir ao Shopping", 
        date:'2004-03-27', 
        time: '14:00',
        address:"Shopping SA"
    },
    {
        id:2, 
        title: "Ir as barraquinhas do Itaú", 
        date:'2000-07-24', 
        time: '08:00',
        address:"Barraquinhas Lendarias 0"
    }];

    const taskPress=(task)=>{
        navigation.navigate('DetalhesDasTarefas', {task});
    }

    return(
        <View>
            <FlatList
            data={tasks}
            keyExtractor={(item) => item.id.toString}
            renderItem={({item}) => (
                <TouchableOpacity onPress={() => taskPress(item)}>
                    <Text>
                        {item.title}
                    </Text>
                </TouchableOpacity>
            )}
            />
        </View>
    )

}

export default TaskList;